#include <stdio.h>
#include <stdlib.h>
#include "network.cpp"
#include "random.h"

int main() {
	int i, j, k;
	int inp_size[3] = {3, 4, 5};
	int inp_num_layers = 3;
	double test_arr_inp[3][9] = 
	{
		{0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5},
		{0.7,0.7,0.7,0.7,0.7,0.7,0.7,0.7,0.7},
		{0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2}
	};
	double test_arr_out[3] = { 5, 7, 2};

	/* Creating the input array. */
	double** test_arr;
	double* test_pred;
	test_arr = (double**)malloc(sizeof(double*) * 3);
	test_pred = (double*)malloc(sizeof(double) * 3);
	for(i = 0; i < 3; i++) {
		test_arr[i] = (double*)malloc(sizeof(double) * 9);
		for(j = 0; j < 9; j++) {
			test_arr[i][j] = test_arr_inp[i][j];
		}
	}
	for(i = 0; i < 3; i++) {
		test_pred[i] = test_arr_out[i];
	}

	/* Creating the network to be used. */
	Network N1(inp_num_layers, inp_size);

	/*
	void SGD(double*** training_data, double* training_out, int len_train_data, int epochs, 
		int mini_batch_size, double eta, double*** test_data = NULL,
		int len_test_data = 0) {
	*/
	N1.SGD(test_arr, test_pred, 3, 4, 1, 0.1);
	printf("Hello World!\n");
	return 0;
}
