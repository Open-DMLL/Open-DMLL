from gi.repository import Gtk
nodes = [("192.0.0.1", "user1", "password"),
        ("192.0.0.2", "user2", "password")]

class MainWindow(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title="Distributed CNN")
        layout = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=8)
        self.add(layout)
        #self.set_size_request(1000, 1000)

        # Convert data to ListStore
        self.node_list = Gtk.ListStore(str, str, str)
        for item in nodes:
            self.node_list.append(list(item))

        node_tree_view = Gtk.TreeView(self.node_list)
        for i, col_title in enumerate(["IP", "Username", "Password"]):
            renderer = Gtk.CellRendererText()
            column = Gtk.TreeViewColumn(col_title, renderer, text=i)
            node_tree_view.append_column(column)

        layout.pack_start(node_tree_view, True, True, 0)

        # IP field
        self.ip_label = Gtk.Label()
        self.ip_label.set_label("Enter IP:")
        self.ip_label.set_halign(Gtk.Align.START)
        layout.pack_start(self.ip_label, True, True, 0)
        self.ip = Gtk.Entry()
        self.ip.set_text("192.0.0.1")
        layout.pack_start(self.ip, True, True, 0)

        # User name
        self.username_label = Gtk.Label()
        self.username_label.set_label("Enter username:")
        self.username_label.set_halign(Gtk.Align.START)
        layout.pack_start(self.username_label, True, True, 0)
        self.username = Gtk.Entry()
        self.username.set_text("MPIUser")
        layout.pack_start(self.username, True, True, 0)

        # Password
        self.password_label = Gtk.Label()
        self.password_label.set_label("Enter password:")
        self.password_label.set_halign(Gtk.Align.START)
        layout.pack_start(self.password_label, True, True, 0)
        self.password = Gtk.Entry()
        self.password.set_visibility(False)
        layout.pack_start(self.password, True, True, 0)

        # Add node button
        self.button_add_node = Gtk.Button(label="Add node")
        self.button_add_node.connect("clicked", self.add_node)
        layout.pack_start(self.button_add_node, True, True, 0)

        # Select program to run
        self.button_add_program = Gtk.Button(label="Add program")
        self.button_add_program.connect("clicked", self.add_program)
        layout.pack_start(self.button_add_program, True, True, 0)

        # Execute program
        self.button_execute = Gtk.Button(label = "Execute")
        self.button_execute.connect("clicked", self.execute)
        layout.pack_start(self.button_execute, True, True, 0)

    def add_node(self, widget):
        self.node_list.append(list([self.ip.get_text(), self.username.get_text(), self.password.get_text()]))

    def add_program(self, widget):
        program_dialog = Gtk.FileChooserDialog("Please select a program", \
                self, Gtk.FileChooserAction.OPEN, \
                ( Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OPEN, Gtk.ResponseType.OK), \
                )
        self.add_filters(program_dialog)
        response = program_dialog.run()
        program_dialog.destroy()

    def add_filters(self, dialog):
        filter_text = Gtk.FileFilter()
        filter_text.set_name("Text files")
        filter_text.add_mime_type("text/plain")
        dialog.add_filter(filter_text)

        filter_py = Gtk.FileFilter()
        filter_py.set_name("Python files")
        filter_py.add_mime_type("text/x-python")
        dialog.add_filter(filter_py)

        filter_any = Gtk.FileFilter()
        filter_any.set_name("Any files")
        filter_any.add_pattern("*")
        dialog.add_filter(filter_any)

    def execute(self, widget):
        pass

window = MainWindow()
window.connect("delete-event", Gtk.main_quit)
window.show_all()
Gtk.main()
