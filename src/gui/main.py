import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk as gtk

# create a window
window = gtk.Window()
window.set_title("Distributed CNN")

# adding an image in the background which requires an overlay
overlay = gtk.Overlay()
window.add(overlay)
background = gtk.Image.new_from_file('white.jpeg')
overlay.add(background)
window.add(overlay)

# create a grid which will hold all the buttons and the labels
grid = gtk.Grid()

# create buttons and label objects
label = gtk.Label("DIGIT RECOGNITION")	
button1 = gtk.Button("UPLOAD...")
button1.set_size_request(130, 60)
button2 = gtk.Button("CAPTURE")
button2.set_size_request(130, 60)
button3 = gtk.Button("LIVE CAMERA")
button3.set_size_request(130, 60)
button4 = gtk.Button("GESTURE")
button4.set_size_request(130, 60)
exit = gtk.Button("EXIT")
exit.set_size_request(130, 60)

# fix the buttons and labels on the window
fixed = gtk.Fixed()
fixed.put(button1, 1700, 110)
fixed.put(button2, 1700, 310)
fixed.put(button3, 1700, 510)
fixed.put(button4, 1700, 710)
fixed.put(exit, 1700, 910)
fixed.put(label, 900, 30)

# add the fixed object to the grid
grid.add(fixed)

# link the buttons and their event functions
#button1.connect("clicked", upload_file_from_system)
#button2.connect("clicked", capture_image_from_webcam)
#button3.connect("clicked", predict_live_from_webcam)
#button4.connect("clicked", draw_the_digit_by_hand)
#exit.connect("clicked", close_window)


# run the window
overlay.add_overlay(grid)
#window.add(fixed)
window.connect("destroy", gtk.main_quit)
window.show_all()
gtk.main()
