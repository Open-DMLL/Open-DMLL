#include <iostream>
using namespace std;
void dot(double* weight, int a, int b, double* activation, int m, int n, double** result) {
        int i, j;
        float sum;
        double* z;
        z = (double*)malloc(sizeof(double) * m);
        //double** ;
        //cout << "here";
        for(i = 0; i < b; i++) {
                sum = 0;
                for(j = 0; j < m;j++) {
                        sum += weight[b * i + j] * activation[j];
                }
                z[i] = sum;
        }
        *result = z;
        return;
}

double reducer(double x) {
        return x;
}

double* sigmoid(double* z, int n) {
        int i;
        double* temp;
        temp = (double*)malloc(sizeof(int) * n);
        for(i = 0;i < n; i++) {
                temp[i] = reducer(z[i]);
        }
        return temp;
}

double* sigmoid_prime(double *z, int n) {
        int i;
        double* temp;
        temp = (double*)malloc(sizeof(int) * n);
        for(i = 0;i < n; i++) {
                temp[i] = reducer(z[i]) * ( 1 - reducer(z[i]));
        }
        return temp;
}
