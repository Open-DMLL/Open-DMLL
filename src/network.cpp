#include <bits/stdc++.h>
#include <random.h>
#include <extra.h>
using namespace std;
class Network {
	public: 
	int num_layers;
	int* sizes;
	double** biases;
	double** weights;
	Network(int inp_num_layers, int* inp_size) {
		int i, j;

		num_layers = inp_num_layers;
		sizes = inp_size;
		biases = (double**)malloc(sizeof(double*) * (num_layers - 1));
		weights = (double**)malloc(sizeof(double*) * (num_layers - 1));

		/*Print Sizes*/
		cout << "Layers: ";
		cout << "[";
		for(int i = 0; i < num_layers; i++) {
			cout << sizes[i] << ", ";
		}
		cout << "]" << endl;


		/*get random values for bias.*/
		for(i = 0; i < num_layers - 1; i++) {
			biases[i] = get_rand_array(sizes[i + 1], 1);
		}

		/*get random values for weights of all layers.*/
		for(i = 0; i < num_layers - 1; i++) {
			weights[i] = get_rand_array(sizes[i], sizes[i + 1]);
		}

		/*Print the value of bias and weights.*/
		cout << "Biases:" <<endl;
		for(int i = 0; i < num_layers - 1; i++) {
			for(j = 0; j < sizes[i + 1]; j++) {
				cout << biases[i][j] << " ";
			}
			cout << endl;
		}
		
		cout << "Weights:" << endl;
		for(int i = 0; i < num_layers - 1; i++) {
			for(j = 0; j < (sizes[i] * sizes[i + 1]); j++) {
				cout << biases[i][j] << " ";
			}
			cout << endl;
		}
	};

	void SGD(double** training_data, double* test_pred,
		 int len_train_data, int epochs, 
		 int mini_batch_size, double eta, double** test_data = NULL,
		 int len_test_data = 0) {

		int i, j, k, l, m, n, no_of_mini_batches;
		double** mini_batch;
		double* mini_batch_pred;
		n = len_train_data;
		// Repeat for number of epochs.
		for(i = 0; i < epochs; i++) {
			// Code to be executed in each epoch.
			randomize(training_data, test_pred, n);	
		}

		/* Print Randomized Training Data. */
		cout << "Training Data: " << endl;
		for(i = 0; i < len_train_data; i++) {
			for(j = 0; j < 9; j++) {
				cout << training_data[i][j] << " ";
			}
			cout << test_pred[i] << " ";
			cout << " " << endl;
		}

		mini_batch = (double**)malloc(sizeof(double*) * mini_batch_size);
		mini_batch_pred = (double*)malloc(sizeof(double) * mini_batch_size);
		cout << "MINI BATCHES: " << endl;
		for(i = 0; i < n; i += mini_batch_size) {
			
			/* Compute the current mini batch. */
			for(j = 0; j < mini_batch_size; j++) {
				mini_batch[j] = training_data[i + j];
				mini_batch_pred[j] = test_pred[i + j];
			}

			this->update_mini_batch(mini_batch, mini_batch_pred, mini_batch_size, eta);
		}
	};

	void update_mini_batch(double **mini_batch,
			       double *mini_batch_pred,
			       int mini_batch_size,
			       double eta) {
	/* 
	 *	Update the network's weights and biases by applying
	 *	gradient descent using backpropagation to a single mini batch.
	 *	The mini_batch is a list of tuples (x, y) and eta
	 *	is the learning rate.
       	*/
		int i, j, k, l, m;
		double **nabla_b, **nabla_w;

		/* Initialize nabla_b & nabla_w to zero array. */
		nabla_b = (double**)malloc(sizeof(double*) * (this->num_layers - 1));
		nabla_w = (double**)malloc(sizeof(double*) * (this->num_layers - 1));
		for(i = 0; i < this->num_layers - 1; i++) {
			nabla_b[i] = get_zero_array(this->sizes[i + 1], 1);
		}

		for(i = 0; i < this->num_layers - 1; i++) {
			nabla_w[i] = get_zero_array(this->sizes[i], this->sizes[i + 1]);
		}

		cout << "Learning Rate : " << eta << endl;

		/* Loop through the mini batch. */
		for(k = 0; k < mini_batch_size; k++) {
			backprop(mini_batch[k], mini_batch_pred[k]);
			cout << "in mini batch iter: " << k << endl;
		}

		/* Print the current mini_batch. */
		for(k = 0; k < mini_batch_size; k++) {
			for(l = 0; l < 3; l++) {
				cout << mini_batch[k][l] << " ";
			}
			cout << mini_batch_pred[k] << " ";
			cout << " " << endl;
		}

	};
	
	/* Return values nabla_b, nabla_w representing the gradient,
	 * for the cost function C_x. */
	void backprop(double* x, double y) {
		int i;
        int j;
		double **nabla_b, **nabla_w;
		double *activation;
        double *z, *delta, *sp;
		double **activations;
		int act_index;
        double **zs;
        int zs_index;
		/* Initialize nabla_b & nabla_w to zero array. */
		nabla_b = (double**)malloc(sizeof(double*) * (this->num_layers - 1));
		nabla_w = (double**)malloc(sizeof(double*) * (this->num_layers - 1));

		/* Initialize activations array and set index to 0. */
		activations = (double**)malloc(sizeof(double*) * (this->num_layers - 1));
		act_index = 0;

        /* Array to store all z-vectors layer by layer. */
        zs = (double**)malloc(sizeof(double*) * this->num_layers);
        zs_index = 0;

		for(i = 0; i < this->num_layers - 1; i++) {
			nabla_b[i] = get_zero_array(this->sizes[i + 1], 1);
		}

		for(i = 0; i < this->num_layers - 1; i++) {
			nabla_w[i] = get_zero_array(this->sizes[i], this->sizes[i + 1]);
		}

		activation = x;
		activations[act_index++] = activation;

		for(i = 0; i < this->num_layers - 1; i++) {
            dot((this->weights)[i],this->sizes[i], this->sizes[i+1], activation, this->sizes[i], 1, &z);
            for(j = 0; j < this->sizes[i]; j++) {
                   z[j] = z[j] + this->biases[i][j];
                   cout << z[j] << " ";
            }
            cout << endl;
            zs[zs_index++] = z;
            activation = sigmoid(z, this->sizes[i]);
            activations[act_index++] = activation;
		}
        //auto delta = (activations[act_index - 1] - y);
        delta = cost_derivative(activations[act_index - 1], this->sizes[i], y, zs[zs_index - 1]);
        nabla_b[this->num_layers - 2] = delta;
        dot(delta, this->sizes[i], 1, activations[act_index - 2], 1, this->sizes[i], &(nabla_w[this->num_layers -2]));

        for(i = 2; i < this->num_layers; i++) {
                z = zs[zs_index - i];
                sp = sigmoid_prime(z, this->sizes[(this->num_layers -1) - i]);
                dot(this->weights[this->num_layers - i],this->sizes[i+1], this->sizes[i], delta, this->sizes[i], 1, &delta);
                nabla_b[(this->num_layers -1) - i] = delta;
                dot(delta, this->sizes[i], 1, activations[(act_index-1) - i], 1, this->sizes[i], &(nabla_w[(this->num_layers-1) - i]));
        }
	}

    double* cost_derivative(double* activation, int n, double y, double* z) {
        int i;
        double* temp;
        temp = (double*)malloc(sizeof(int) * n);
        for(i = 0; i < n; i++) {
               temp[i] = (activation[i] - y) * (reducer(z[i]) * (1 - reducer(z[i])));
        }
        return temp;
    }
};
