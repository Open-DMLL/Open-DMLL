#include "distributed.h"

int main() {
	int num_layers = 3;
	int layer_sizes[] = { 3, 5, 3 };
	double a[] = { 1, 0, 0 };
	double b[] = { 0, 1, 0 };
	double c[] = { 0, 0, 1 };
	double *training_data[3] = { a, b, c };
	double oa[] = { 1, 0, 0 },
		ob[] = { 0, 1, 0 },
		oc[] = { 0, 0, 1 };
	double *exp_output[3] = { oa, ob, oc };
	DNNetwork n(num_layers, layer_sizes);
	n.SGD(training_data, exp_output, 3, 3, 1, 3);
}
