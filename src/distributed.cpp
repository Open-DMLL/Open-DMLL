#include "distributed.h"
#include "mpi.h"
#include <bits/stdc++.h>
#include <dnn_random.h>
#include <math.h>
using namespace std;

/*
 * Distribution of work will take place such that that there will be layers of
 * computers. Each layer will correspond to more than one layer of neural network.
 * Within each layer of computers, each computer will be responsible for some nodes
 * of each layer of NN
 *
 * all of the following assumes input layer as also one of the layer
 * num_layers -> all calculation layer + input layer + output layer
 * number of layers start from 0
 * layer_sizes -> contain sizes starting from size of input layer so
 * 	0 -> input layer
 * 	1 -> first layer and so on
 * biases -> contain biases for layers other than input layer so
 * 	0 -> first layer and so on
 * weights[i] -> contain weights between i+1 and i layer. although we represent
 * 	weights in theory as 2d array here we have used 1d as its simpler to transfer
 * 	using MPI. so in 2d representation row represent nodes of i+1th layer and column
 * 	represent nodes of ith layer
 */
// TODO check in literature if input layer is also consider as one of the
// nn layers; for now assuming yes
DNNetwork::DNNetwork(int inp_num_layers, int inp_size[]) {
	MPI_Init(NULL, NULL);
	MPI_Comm_size(MPI_COMM_WORLD, &num_procs);
	MPI_Comm_rank(MPI_COMM_WORLD, &my_id);

	// if this is not a perfect integer, remaning nn layers will
	// be computed by last layer of computers
	num_nnlayer = inp_num_layers/num_procs;

	num_layers = inp_num_layers;
	layer_sizes = Map<VectorXi>(inp_size, inp_num_layers);

	biase_start_i = weights_start_i = my_id * num_nnlayer;
	biase_end_i = weights_end_i = (my_id + 1) * num_nnlayer - 1;
	biase_size = biase_end_i - biase_start_i + 1;
	weights_size = weights_end_i - weights_start_i + 1;

	if(my_id == 0) {
		// its hard to generate random weights and biases distributedly
		// hence for now generating it on one computer
		// TODO distribute this
		// TODO change the following when input layer issue is solved
		// change wherever num_layers - 1 occurs
		biases = (MatrixXd *)malloc(sizeof(MatrixXd) * (num_layers - 1));
		weights = (MatrixXd *)malloc(sizeof(MatrixXd) * (num_layers - 1));

		/*get random values for bias.*/
		/*get random values for weights of all layers.*/
		for(int i = 0; i < num_layers - 1; i++) {
			biases[i] = MatrixXd::Random(layer_sizes[i + 1], 1);
			weights[i] = MatrixXd::Random(layer_sizes[i], layer_sizes[i + 1]);
		}

		for(int i = 1; i < num_procs; i++) {
			// sending layers to their corresponding process
			for(int j = 0; j < num_nnlayer; j++) {
				int layer_i = i * num_nnlayer + j - 1;
				MPI_Send(biases[layer_i].data(), layer_sizes[layer_i + 1],
						MPI_DOUBLE, i, layer_i, MPI_COMM_WORLD);
				MPI_Send(weights[layer_i].data(), layer_sizes[layer_i + 1] * layer_sizes[layer_i + 2],
						MPI_DOUBLE, i, layer_i, MPI_COMM_WORLD);
			}
		}
		// sending remaining layers to the last process
		for(int i = 0; i < num_layers%num_procs; i++) {
			int layer_i = num_procs * num_nnlayer + i - 1;
			MPI_Send(biases[layer_i].data(), layer_sizes[layer_i + 1], MPI_DOUBLE,
					num_procs - 1, layer_i, MPI_COMM_WORLD);
			MPI_Send(weights[layer_i].data(), layer_sizes[layer_i + 1] * layer_sizes[layer_i + 2],
						MPI_DOUBLE, num_procs - 1, layer_i, MPI_COMM_WORLD);
		}
	}
	else {
		// call function which will handle computation part
		// if I am the last process, I have the responsibility of handling
		// last layers
		if( my_id == num_procs - 1) {
			// dont increase num_nnlayer for last layer special case
			// as it is use as number of layers in each computer
			// whenever num layers in this comp needed use bias_size
			// num_nnlayer += num_layers % num_procs;
			biase_end_i += num_layers % num_procs;
			weights_end_i = num_layers % num_procs;
			biase_size = biase_end_i - biase_start_i + 1;
			weights_size = weights_end_i - weights_start_i + 1;
		}
		biases =  (MatrixXd *) malloc(sizeof(MatrixXd) * biase_size);
		weights = (MatrixXd *) malloc(sizeof(MatrixXd) * biase_size);
		for(int i = 0; i < biase_size; i++) {
			int layer_i = my_id * num_nnlayer + i - 1;
			biases[i] =  MatrixXd(layer_sizes[layer_i + 1], 1);
			weights[i] = MatrixXd(layer_sizes[layer_i + 1], layer_sizes[layer_i + 2]);
			MPI_Recv(biases[i].data(), layer_sizes[layer_i + 1], MPI_DOUBLE, 0, layer_i,
					MPI_COMM_WORLD, NULL);
			MPI_Recv(weights[i].data(), layer_sizes[layer_i + 1] * layer_sizes[layer_i + 2],
						MPI_DOUBLE, 0, layer_i, MPI_COMM_WORLD, NULL);
		}
	}
}

void DNNetwork::SGD(double *training_data[],
		double *train_expec_output[],
		int len_train_data,
		int epochs, int mini_batch_size, double eta,
		double *test_data[],
		double *test_expec_output[],
		int len_test_data) {
	for(int i = 0; i < epochs; i++) {
		shuffle_array(training_data, training_data + len_train_data,
					train_expec_output, train_expec_output + len_train_data);
		for(int j = 0; j < len_train_data / mini_batch_size; j++) {
			this->update_mini_batch(training_data + j * mini_batch_size,
					train_expec_output + j * mini_batch_size,
					mini_batch_size, eta);
		}
		if(len_test_data != 0) {
			cout << "Epoch " << i << " " << this->evaluate(test_data, test_expec_output,
					len_test_data) << " / "
				<< len_test_data << endl;
		}
		else {
			cout << "Epoch " << i << endl;
		}
	}
}
int DNNetwork::evaluate(double **input_batch, double **exp_output, int size) {

	MatrixXd activation, z, y;
	int result = 0;
	for(int i = 0; i < size; i++) {
		if(my_id == 0) {
			activation = Map<MatrixXd>(input_batch[i], layer_sizes[0], 1);
		}
		else {
			activation = MatrixXd::Zero(layer_sizes[my_id * num_nnlayer - 2], 1);
		}
		// activation = input_from_prev_layer();
		for(int j = 0; j < biase_size; j++) {
			z = weights[j] * activation + biases[j];
			activation = this->sigmoid(z);
			// activation.unaryExpr([](double x) { return sigmoid(x) });
		}
		// send activation to next layer
		if(my_id != num_procs - 1) {
			MPI_Send(activation.data(), activation.rows(), MPI_DOUBLE,
				my_id + 1, i, MPI_COMM_WORLD);
		}
		else {
			y = Map<MatrixXd>(exp_output[i], layer_sizes[num_layers - 1], 1);
			// see which of activation is maximum and check if y also has
			// 1 in that place
			double tmp = activation.maxCoeff();
			for(int i = 0; i < activation.rows(); i++) {
				if(activation(i, 0) == tmp) {
					if(y(i, 0) == 1) {
						result++;
					}
					else {
						break;
					}
				}
			}
		}
	}
	return result;
}
void DNNetwork::update_mini_batch(double **mini_batch, double **mini_batch_output,
						int mini_batch_size, double eta) {
	MatrixXd *nabla_b, *nabla_w, *activations, *zs, delta, sp, activation, y;
	nabla_b = (MatrixXd *)malloc(sizeof(MatrixXd) * biase_size);
	nabla_w = (MatrixXd *)malloc(sizeof(MatrixXd) * weights_size);
	activations = (MatrixXd *)malloc(sizeof(MatrixXd) * biase_size);
	zs = (MatrixXd *)malloc(sizeof(MatrixXd) * biase_size);
	for(int i = 0; i < biase_size; i++) {
		int layer_i = my_id * num_nnlayer + i - 1;
		nabla_b[i] = MatrixXd::Zero(layer_sizes[layer_i + 1], 1);
		nabla_w[i] = MatrixXd::Zero(layer_sizes[layer_i + 1],
									layer_sizes[layer_i + 2]);
		activations[i] = MatrixXd::Zero(layer_sizes[layer_i + 1], 1);
		zs[i] = MatrixXd::Zero(layer_sizes[layer_i + 1], 1);

	}

	for(int i = 0; i < mini_batch_size; i++) {
		if(my_id == 0) {
			activation = Map<MatrixXd>(mini_batch[i], layer_sizes[0], 1);
		}
		else {
			activation = MatrixXd::Zero(layer_sizes[my_id * num_nnlayer - 2], 1);
		}
		// activation = input_from_prev_layer();
		for(int j = 0; j < biase_size; j++) {
			activations[j] = activation;
			zs[j] = weights[j] * activation + biases[j];
			activation = this->sigmoid(zs[j]);
			// activation.unaryExpr([](double x) { return sigmoid(x) });
		}
		// send activation to next layer
		if(my_id != num_procs - 1) {
			MPI_Send(activation.data(), activation.rows(), MPI_DOUBLE,
				my_id + 1, i, MPI_COMM_WORLD);
		}
		else {
			y = Map<MatrixXd>(mini_batch_output[i], layer_sizes[num_layers - 1], 1);
			delta = (activation - y) * sigmoid_prime(zs[biase_size - 1]);
		}
		// output_to_next_layer();
	}
	for(int i = 0; i < mini_batch_size; i++) {
		// wait for receiving delta
		if(my_id != num_procs - 1) {
			delta = MatrixXd::Zero(layer_sizes[(my_id + 1) * num_nnlayer], 1);
			MPI_Recv(delta.data(), delta.rows(), MPI_DOUBLE, my_id + 1, i,
					MPI_COMM_WORLD, NULL);
		}
		// for each layer on this computer
		// calculate nabla in biase and weight
		for(int j = biase_size - 1; j >= 0; j--) {
			sp = sigmoid_prime(zs[j]);
			// here assuming delta is dot(weights[j + 1].transpose, orig_delta)
			delta *= sp;
			nabla_b[j] += delta;
			nabla_w[j] += delta * activations[j].transpose();
			delta = weights[j].transpose() * delta;
		}
		// pass the delta to previous layer
		// backprop_send_delta(delta);
		MPI_Send(delta.data(), delta.rows(), MPI_DOUBLE, my_id - 1, i,
				MPI_COMM_WORLD);
	}
	for(int i = 0; i < biase_size; i++) {
		weights[i] = weights[i] - (eta/mini_batch_size) * nabla_w[i];
		biases[i] = biases[i] - eta/mini_batch_size * nabla_b[i];
	}
}
MatrixXd DNNetwork::sigmoid(MatrixXd z) {
	z.unaryExpr([](double x) { return 1/(1 + exp(-x)); });
	return z;
}
MatrixXd DNNetwork::sigmoid_prime(MatrixXd z) {
	MatrixXd a = z;
	this->sigmoid(a);
	z = (a * (a.unaryExpr([](double x) { return 1 - x;})));
	return z;
}
