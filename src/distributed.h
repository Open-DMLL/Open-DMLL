#include <eigen3/Eigen/Core>
using namespace Eigen;

class DNNetwork {
	public:
	int num_procs, my_id;				// number of processes running in the cluster
										// and my id in this cluster

	int num_layers;						// number of nn layers
	VectorXi layer_sizes;					// number of nodes in each nn layer
	MatrixXd *biases;					// 1D array of pointers to biases of each nn layer
	MatrixXd *weights;					// 1D array of pointers to 1D array of weights of each nn layer
	int biase_start_i, biase_end_i;		// start and end index of layer_sizes which are allocated to
	int weights_start_i, weights_end_i; // this pc
	int biase_size, weights_size;
	int num_nnlayer;

	// TODO check in literature if input layer is also consider as one of the
	// nn layers; for now assuming yes
	DNNetwork(int inp_num_layers, int inp_size[]);
	void SGD(double *training_data[],
			double *train_expec_output[],
			int len_train_data,
			int epochs, int mini_batch_size, double eta,
			double *test_data[] = NULL,
			double *test_expec_output[] = NULL,
			int len_test_data = 0);
	int evaluate(double **input_batch, double **exp_output, int size);
	void update_mini_batch(double **mini_batch, double **mini_batch_output,
							int mini_batch_size, double eta);
	MatrixXd sigmoid(MatrixXd z);
	MatrixXd sigmoid_prime(MatrixXd z);
};
