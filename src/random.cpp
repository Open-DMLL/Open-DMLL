#include <stdio.h>
#include <stdlib.h>
#include <bits/stdc++.h>
#include <random>
#include <cstdlib>
#include <cmath>
#include <ctime>
#include <iostream>
using namespace std;

double get_rand() {
	std::mt19937 generator;
	double mean = 0.0;
	double stddev  = 1.0;
	double rand_val;
	std::normal_distribution<double> normal(mean, stddev);
	rand_val = normal(generator);
}

double RandomGenerator()
{
  return ( (double)(rand()) + 1. )/( (double)(RAND_MAX) + 1. );
}
 // return a normally distributed random number
double normalRandom()
{
  double y1=RandomGenerator();
  double y2=RandomGenerator();
  cout << y1 << endl;
  cout << y2 << endl;
  return cos(2*3.14*y2)*sqrt(-2.*log(y1));
}

double* get_rand_array(int row, int col) {
	int i;
	int n = row * col;
	double *arr;
	arr = (double*)malloc(sizeof(double) * n);
	for(i = 0; i < n; i++) {
		arr[i] = RandomGenerator();
	}
	return arr;
}

double* get_zero_array(int row, int col) {
	int i;
	int n = row * col;
	double *arr;
	arr = (double*)malloc(sizeof(double) * n);
	memset(arr, 0, sizeof(double) * n);
	return arr;
}

void randomize(double** arr, double* arr_out, int len) { // Returns a randomized version of the arr array.
	double seed;	
	seed = RandomGenerator();
	shuffle(arr, arr + len, default_random_engine(seed));
	shuffle(arr_out, arr_out + len, default_random_engine(seed));
}
