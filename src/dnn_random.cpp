#include "dnn_random.h"
#include <algorithm>
#include <random>
void shuffle_array(double **a_start, double **a_end, double **b_start, double **b_end) {
	double seed = rand();
	shuffle(a_start, a_end, std::default_random_engine(seed));
	shuffle(b_start, b_end, std::default_random_engine(seed));
}
