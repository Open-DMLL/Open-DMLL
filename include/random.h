double get_rand();
double RandomGenerator();
double normalRandom();
double* get_rand_array(int row, int col);
double* get_zero_array(int row, int col);
void randomize(double** arr, double* arr_out, int len);
